variable region_name {
    default = "us-west-2"
}
variable state_file_bucket_name {}
variable dynamo_db_table_name {
    description  = "terraform state locking file"
}
# variable ecr_repo_ui_name {}
# variable ecr_repo_api_name {}

# variable "user_pool_name" {}
# variable "sns_caller_arn" {}
# variable "sns_external_id" {}
# variable "lambda_auth_challenge" {}
# variable "verify_auth_challenge_response" {}
