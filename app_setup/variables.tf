variable "region_name" {
    description = "Region Name"
    default = "us-west-2"
}

variable "vpc_cidr_block" {
    description = "CIDR block for VPC"
}
variable "open_cidr" {}

variable "environment" {
    description = "environment name"
}
variable "public_subnet_cidr_block1" {
    description = "CIDR block for Public Subnet1"
}

variable "private_subnet_cidr_block1" {
    description = "CIDR block for Private Subnet1"
}


variable "public_subnet_cidr_block2" {
    description = "CIDR block for Public Subnet2"
}

variable "private_subnet_cidr_block2" {
    description = "CIDR block for Private Subnet2"
}


variable "public_subnet_cidr_block3" {
    description = "CIDR block for Public Subnet3"
}

variable "private_subnet_cidr_block3" {
    description = "CIDR block for Private Subnet3"
}
variable "backend_api_port" {
    description = "backend_api_port"
}
variable "ui_service_sg_name" {
    description = "ui_service_sg_name"
}
variable "ui_loadbalancer_sg_name" {
    description = "ui_alb_sg_name"
}
variable "api_service_sg_name" {
    description = "api_service_sg_name"
}
variable "api_loadbalancer_sg_name" {
    description = "api_alb_sg_name"
}
variable "org_prefix" {
    description = "org prefix name"
} 
variable "db_identifier_name" {
    description = "db_identifier_name"
}
variable "db_username"{}
variable "db_password"{}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_subnetgroup_name" {}
variable "db_instance_class" {}
variable "db_name" {}
variable "db_allocated_storage" {}
variable "db_port" {}


variable "bitbucket_branch" {}
variable "bitbucket_username" {}

variable "ui_container_name" {}

variable "ui_container_port" {}

variable "ui_task_cpu" {}
variable "ui_task_memory" {}
variable "api_container_name" {}

variable "api_container_port" {}
variable "api_task_cpu" {}
variable "api_task_memory" {}

variable "ui_container_port_mappings" {
  type = list(object({
    hostPort      = number
    containerPort = number
    protocol      = string
  }))
  default = [{
    hostPort      = 80
    containerPort = 80
    protocol      = "tcp"
  }]
}
variable "api_container_port_mappings" {
  type = list(object({
    hostPort      = number
    containerPort = number
    protocol      = string
  }))
  default = [{
    hostPort      = 4000
    containerPort = 4000
    protocol      = "tcp"
  }]
}

variable "logConfiguration_api" {
  default     = {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "awslogs-api",
        "awslogs-region": "us-west-2",
        "awslogs-stream-prefix": "ecs"
        }
    }
  description = "The log configuration specification for the container"
  type        = any
}

 variable "sns_subscription_protocol" {
   type = string
   default = "email"
   description = "SNS subscription protocal"
 }

variable "sns_subscription_email_address_list_pipeline" {
  type = string
  description = "List of email addresses as string(comma separated)"
}

variable "ecr_ui_repository_url" {}
variable "ecr_api_repository_url" {}

variable "user_pool_name" {}
variable "sns_caller_arn" {}
variable "sns_external_id" {}
variable "lambda_auth_challenge" {}
variable "verify_auth_challenge_response" {}

variable "state_file_bucket_name" {}
variable "dynamo_db_table_name" {
    description  = "terraform state locking file"
}

variable "ecr_repo_ui_name" {}
variable "ecr_repo_api_name" {}

