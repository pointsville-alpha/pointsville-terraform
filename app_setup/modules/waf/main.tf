#WAF REGIONAL RULE FOR IP BLACKLISTED IPS
resource "aws_wafregional_rule" "detect_blacklisted_ips" {
  depends_on = [aws_wafregional_ipset.blacklisted_ips]

  name        = "Detect-blacklisted"
  metric_name = "genericdetectblacklistedips"
  predicate {

    data_id = aws_wafregional_ipset.blacklisted_ips.id
    negated = false
    type    = "IPMatch"
  }
}

#WAF IP BLACKLISTED IPS
resource "aws_wafregional_ipset" "blacklisted_ips" {

  name = "blacklisting_ips"

  dynamic "ip_set_descriptor" {

    iterator = x
    for_each = var.rule_ip_blacklist_ipv4
    content {
      type  = "IPV4"
      value = x.value
    }
  }
}

#WAF REGIONAL RULE DEFINITION FOR SQL INJECTION
resource "aws_wafregional_sql_injection_match_set" "pointsville_01_sql_injection_set" {

  name = "SQLINJECTIONSET"

  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "URI"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "URI"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "BODY"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "BODY"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "HEADER"
      data = "Authorization"
    }
  }

  sql_injection_match_tuple {
    text_transformation = "HTML_ENTITY_DECODE"

    field_to_match {
      type = "HEADER"
      data = "Authorization"
    }
  }
}

#WAF REGIONAL RULE FOR SQL INJECTION
resource "aws_wafregional_rule" "pointsville_01_sql_injection_rule" {
  depends_on  = [aws_wafregional_sql_injection_match_set.pointsville_01_sql_injection_set]
  name        = "SQLINJECTIONRULENAME"
  metric_name = "SQLINJECTIONRULE"

  predicate {
    data_id = aws_wafregional_sql_injection_match_set.pointsville_01_sql_injection_set.id
    negated = "false"
    type    = "SqlInjectionMatch"
  }
}

