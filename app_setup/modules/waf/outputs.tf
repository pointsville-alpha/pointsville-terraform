output "detect_blacklistips" {
    description =   "Detect Black List IPS"
    value       =   aws_wafregional_rule.detect_blacklisted_ips
}

output "blacklistips" {
    description = "Black List IPS"
    value = aws_wafregional_ipset.blacklisted_ips  
}

output "sqlInjection" {
    description = "SQL Injection Rule"
    value = aws_wafregional_rule.pointsville_01_sql_injection_rule  
}