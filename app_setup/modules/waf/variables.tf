variable "rule_ip_blacklist_ipv4" {
  type        = list(string)
  description = "A blacklist of IPV4 cidr blocks"
  default     = ["157.48.222.64/32", "139.5.250.24/32", "139.5.250.23/32"]
}