# Note: Based on Congnito User pool & Identidy pool set up documentation using Amplify, we need to set up these pools using Amplify which uses CloudFormation.
/*
resource "aws_cognito_user_pool" "pv-user-pool" {
    name                       = var.user_pool_name
    auto_verified_attributes   = ["email"]
    email_verification_subject = "Verify your PointsVille email address."
    email_verification_message = "<div>\n<h1 style=\"font-size:32px;line-height:36px;font-weight:500;padding-bottom:10px;color:#333;text-align:center\">Verify your PointsVille email address.</h1>\n\t\t\t<div style=\"font-size:17px;line-height:25px;color:#333;font-weight:normal\">\n\t\t\t\t\t\t\t\t\t<p></p>\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">You have selected this email address as your new <span style=\"white-space:nowrap\">PointsVille ID</span>. To verify this email address belongs to you, enter the code below on the verification page:</p>\n\t\t\t\t\t\t\t<p style=\"font-size:23px;line-height:25px;color:#333;font-weight:normal;color:#333;text-align:center\">\n\t\t\t\t\t\t\t\t<b>{####}</b>\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">\n\t\t\t\t\t\t\t<b style=\"color:#333;text-align:center\">Why you received this email.</b><br>\n\t\t\t\t\t\tPointsVille requires verification whenever an email address is selected as an <span style=\"white-space:nowrap\">PointsVille ID</span>. Your <span style=\"white-space:nowrap\">PointsVille ID</span> cannot be used until you verify it.</p>\n\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">If you did not make this request, you can ignore this email.</p>\n\t\t\t\t\t\t<p></p>\n\t\t\t </div>\n      \t\t</div>"
    mfa_configuration          = "OFF"
    sms_authentication_message = "Your authentication code is {####}. "
    sms_verification_message   = "Your verification code is {####}"
    username_attributes        = ["email"]

    email_configuration {
        email_sending_account = "COGNITO_DEFAULT"
    }
    device_configuration {
        challenge_required_on_new_device = false
        device_only_remembered_on_user_prompt = true
    }
    verification_message_template {
        sms_message = "Your verification code is {####}"
        email_message = "<div>\n<h1 style=\"font-size:32px;line-height:36px;font-weight:500;padding-bottom:10px;color:#333;text-align:center\">Verify your PointsVille email address.</h1>\n\t\t\t<div style=\"font-size:17px;line-height:25px;color:#333;font-weight:normal\">\n\t\t\t\t\t\t\t\t\t<p></p>\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">You have selected this email address as your new <span style=\"white-space:nowrap\">PointsVille ID</span>. To verify this email address belongs to you, enter the code below on the verification page:</p>\n\t\t\t\t\t\t\t<p style=\"font-size:23px;line-height:25px;color:#333;font-weight:normal;color:#333;text-align:center\">\n\t\t\t\t\t\t\t\t<b>{####}</b>\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">\n\t\t\t\t\t\t\t<b style=\"color:#333;text-align:center\">Why you received this email.</b><br>\n\t\t\t\t\t\tPointsVille requires verification whenever an email address is selected as an <span style=\"white-space:nowrap\">PointsVille ID</span>. Your <span style=\"white-space:nowrap\">PointsVille ID</span> cannot be used until you verify it.</p>\n\t\t\t\t\t\t<p style=\"color:#333;text-align:center\">If you did not make this request, you can ignore this email.</p>\n\t\t\t\t\t\t<p></p>\n\t\t\t </div>\n      \t\t</div>"
        email_subject = "Verify your PointsVille email address."
        email_message_by_link = "Please click the link below to verify your email address. {##Verify Email##} "
        email_subject_by_link = "Your verification link"
        default_email_option = "CONFIRM_WITH_CODE"
    }
    password_policy {
        minimum_length = 8
        require_uppercase = false
        require_lowercase = false
        require_numbers = false
        require_symbols = false
        temporary_password_validity_days = 7
    }
    sms_configuration {
        sns_caller_arn = var.sns_caller_arn
        external_id = var.sns_external_id
    }
    admin_create_user_config {
        allow_admin_create_user_only = false
        invite_message_template {
            sms_message = "Pointsville : Your username is {username} and temporary password is {####}. "
            email_message = "Pointsville : Your username is {username} and temporary password is {####}. "
            email_subject = "Pointsville : Your temporary password"
        }
    }
    lambda_config {
        define_auth_challenge = var.lambda_auth_challenge
        verify_auth_challenge_response = var.verify_auth_challenge_response
    }

    schema {
        name                     = "sub"
        attribute_data_type      = "String"
        developer_only_attribute = false
        mutable                  = false
        required                 = true
        string_attribute_constraints {
            min_length = 1
            max_length = 2048
        }
    }

    schema {
        name                     = "name"
        attribute_data_type      = "String"
        developer_only_attribute = false
        mutable                  = true
        required                 = true
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "given_name"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "family_name"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "middle_name"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "nickname"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "preferred_username"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "profile"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "picture"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "website"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "email"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = true
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "email_verified"
        attribute_data_type = "Boolean"
        developer_only_attribute = false
        mutable = true
        required = false
    }

    schema {
        name = "gender"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "birthdate"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 10
            max_length = 10
        }
    }

    schema {
        name = "zoneinfo"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "locale"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "phone_number"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "phone_number_verified"
        attribute_data_type = "Boolean"
        developer_only_attribute = false
        mutable = true
        required = false
    }

    schema {
        name = "address"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {
            min_length = 0
            max_length = 2048
        }
    }

    schema {
        name = "updated_at"
        attribute_data_type = "Number"
        developer_only_attribute = false
        mutable = true
        required = false
        number_attribute_constraints {
            min_value = 0
        }
    }

    schema {
        name = "identities"
        attribute_data_type = "String"
        developer_only_attribute = false
        mutable = true
        required = false
        string_attribute_constraints {}
    }
            
}
*/