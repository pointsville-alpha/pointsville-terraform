//creating rds db instance
resource "aws_db_instance" "pointsville-rds-db-instance" {
    allocated_storage    = var.db_allocated_storage
    db_subnet_group_name = aws_db_subnet_group.rds-subnetgroup.id
    engine               = var.db_engine
    engine_version       = var.db_engine_version
    identifier           = "${var.org_prefix}-${var.db_identifier_name}-${var.environment}"
    instance_class       = var.db_instance_class
    password             = var.db_password
    skip_final_snapshot  = true
    storage_encrypted    = true
    username             = var.db_username
    port                 = var.db_port
    name                 = var.db_name
    vpc_security_group_ids   = [ aws_security_group.rds-sg.id ]
    tags = {
        name        = "${var.org_prefix}-${var.db_identifier_name}-${var.environment}"
        Environment = var.environment
    }
    depends_on = [ aws_db_subnet_group.rds-subnetgroup ]
}

//creating rds subnet group
resource "aws_db_subnet_group" "rds-subnetgroup" {
    name        = "${var.org_prefix}-${var.db_subnetgroup_name}-${var.environment}"
    description = "Our main group of subnets"
    subnet_ids  = aws_subnet.pointsville-public-subnet.*.id
    depends_on  = [ aws_subnet.pointsville-public-subnet ]
    tags = {
        name        = "${var.org_prefix}-${var.db_subnetgroup_name}-${var.environment}"
        Environment = var.environment
    }
}

// Creating RDS security group and rules,
resource "aws_security_group" "rds-sg" {
    name        = "${var.org_prefix}-rds-sg-${var.environment}"
    vpc_id      = aws_vpc.pointsville-vpc.id

    tags = {
        Name        = "${var.org_prefix}-rds-sg-${var.environment}"
        Environment = var.environment
    }
    lifecycle {
        create_before_destroy = true
        ignore_changes        = [name]
    }
}

//Creating RDS security group rules
resource "aws_security_group_rule" "rds_sg_in" {
    type                     = "ingress"
    from_port                = var.db_port
    to_port                  = var.db_port
    protocol                 = "tcp"
    source_security_group_id = aws_security_group.pvl-api-alb-sg.id
    security_group_id        = aws_security_group.rds-sg.id
    depends_on = [ aws_security_group.rds-sg ]
}

//Creating RDS security group rule
resource "aws_security_group_rule" "rds_cidr_in" {
    type              = "ingress"
    from_port         = var.db_port
    to_port           = var.db_port
    protocol          = "tcp"
    cidr_blocks       = [ var.vpc_cidr_block ]
    security_group_id = aws_security_group.rds-sg.id
    depends_on = [ aws_security_group.rds-sg ]
}