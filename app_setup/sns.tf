locals {
  endpoints = split(",", var.sns_subscription_email_address_list_pipeline)
}

resource "null_resource" "subscriptions_mapping" {
  count = length(local.endpoints)

  triggers = {
    Endpoint = element(local.endpoints, count.index)
    Protocol = var.sns_subscription_protocol
  }
}

data "template_file" "aws_cf_sns_stack_codepipeline" {
  depends_on = [ null_resource.subscriptions_mapping ]


  template = file("${path.module}/templates/cf_aws_sns_email_stack.json.tpl")
  vars = {
    sns_topic_name        = "${var.org_prefix}-pipeline_notifications"
    sns_display_name      = "${var.org_prefix}-pipeline_notifications"
    sns_subscription_list = jsonencode(null_resource.subscriptions_mapping.*.triggers)
  }
}
 
resource "aws_cloudformation_stack" "tf_sns_topic_codepipeline" {
  name = "sns-stack-codepipeline"
  template_body = data.template_file.aws_cf_sns_stack_codepipeline.rendered
  tags = {
    name = "sns-stack-codepipeline"
  }
}

data "aws_sns_topic" "sns-topic-pipeline-notifications" {
  depends_on = [ aws_cloudformation_stack.tf_sns_topic_codepipeline ]
  name = "${var.org_prefix}-pipeline_notifications"
}

resource "aws_cloudwatch_event_rule" "codepipeline-event-rule" {
  depends_on = [ 
    aws_cloudformation_stack.tf_sns_topic_codepipeline,
    aws_codepipeline.admin-ui-pipeline,
    aws_codepipeline.admin-backend-pipeline,
    aws_codepipeline.android-pipeline
  ]
  name        = "capture-failed-activities"
  description = "Capture all the failed pipeline activities"

  event_pattern = <<EOF
{
  "source": [
    "aws.codepipeline"
  ],
  "detail-type": [
    "CodePipeline Pipeline Execution State Change"
  ],
  "detail": {
    "state": [
      "FAILED"
    ],
    "pipeline": [
      "${aws_codepipeline.admin-ui-pipeline.name}",
      "${aws_codepipeline.admin-backend-pipeline.name}",
      "${aws_codepipeline.android-pipeline.name}"
    ]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "sns-target" {
  rule      = aws_cloudwatch_event_rule.codepipeline-event-rule.name
  arn       = data.aws_sns_topic.sns-topic-pipeline-notifications.arn
}