// Fetching all availability zones in the region
data "aws_availability_zones" "available" {}

// creating vpc
resource "aws_vpc" "pointsville-vpc" {
    cidr_block = var.vpc_cidr_block
    enable_dns_support   = true 
    enable_dns_hostnames = true    
    tags = {
        Name = "${var.org_prefix}-vpc-${var.environment}"
        Environment = var.environment
    }
}

//creating public subnets for each AZ
resource "aws_subnet" "pointsville-public-subnet" {
    count = length(data.aws_availability_zones.available.names)
    vpc_id = aws_vpc.pointsville-vpc.id
    cidr_block = "10.0.${10+count.index}.0/24"
    availability_zone = data.aws_availability_zones.available.names[count.index]
    map_public_ip_on_launch = true
    depends_on = [
      aws_vpc.pointsville-vpc
    ]
    tags = {
      Name = "${var.org_prefix}-public-subnet-${var.environment}"
      Environment = var.environment
    }
}

//creating private subnets for each AZ
resource "aws_subnet" "pointsville-private-subnet" {
  depends_on = [
    aws_vpc.pointsville-vpc
  ]
  count = length(data.aws_availability_zones.available.names)
  vpc_id = aws_vpc.pointsville-vpc.id
  cidr_block = "10.0.${20+count.index}.0/24"
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.org_prefix}-private-subnet-${var.environment}"
    Environment = var.environment
  }
}

//creating internet gateway
resource "aws_internet_gateway" "pointsville-internet-gateway" {
  depends_on = [
    aws_vpc.pointsville-vpc,
  ]
  vpc_id = aws_vpc.pointsville-vpc.id
  tags = {
    Name = "${var.org_prefix}-igw-${var.environment}"
    Environment = var.environment
  }
}

// public route table
resource "aws_route_table" "pointsville-route-table" {
  depends_on = [
    aws_vpc.pointsville-vpc,
    aws_internet_gateway.pointsville-internet-gateway
  ]
  vpc_id = aws_vpc.pointsville-vpc.id
  route {
    cidr_block = var.open_cidr
    gateway_id = aws_internet_gateway.pointsville-internet-gateway.id
  }
  tags = {
    Name = "${var.org_prefix}-public-rtb-${var.environment}"
    Environment = var.environment
  }
}

//Route Table Association for Internet Gateway
resource "aws_route_table_association" "RT-IG-Association" {
  depends_on = [
    aws_vpc.pointsville-vpc,
    aws_subnet.pointsville-public-subnet,
    aws_route_table.pointsville-route-table
  ]
  count = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.pointsville-public-subnet.*.id, count.index)
  route_table_id = aws_route_table.pointsville-route-table.id
}

//creating EIP for natgateway 
resource "aws_eip" "nat-gateway-pubip" {
  vpc = true
}

//creating natgateway for pirvate subnets
resource "aws_nat_gateway" "nat-gateway" {
    allocation_id = aws_eip.nat-gateway-pubip.id
    subnet_id = element(aws_subnet.pointsville-public-subnet.*.id, 1)
    tags = {
        "Name" = "${var.org_prefix}-NATgw-${var.environment}"
        "Environment" = var.environment
    }
    depends_on = [ aws_subnet.pointsville-public-subnet, aws_subnet.pointsville-private-subnet, aws_eip.nat-gateway-pubip ]
}

// private route table for priavte subnets
resource "aws_route_table" "private-natgateway-rtb" {
    vpc_id = aws_vpc.pointsville-vpc.id
    route {
        cidr_block = var.open_cidr
        nat_gateway_id = aws_nat_gateway.nat-gateway.id
    }
    tags = {
        Name = "${var.org_prefix}-natgw-private-rtb-${var.environment}"
        "Environment" = var.environment
    }    
}

//Route Table Association for NAT Gateway
resource "aws_route_table_association" "natgw-private-association" {
    count = length(data.aws_availability_zones.available.names)
    subnet_id      = element(aws_subnet.pointsville-private-subnet.*.id, count.index)
    route_table_id = aws_route_table.private-natgateway-rtb.id

    depends_on = [
    aws_vpc.pointsville-vpc,
    aws_subnet.pointsville-private-subnet,
    aws_route_table.private-natgateway-rtb
  ]
}
