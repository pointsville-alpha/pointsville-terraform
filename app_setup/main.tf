terraform {
  backend "s3" {
    # We need to replace these parameters if changed in initial set up
    bucket         = "pvl-terraform-statefile-qa"
    key            = "global/s3/terraform.tfstate"
    region         =  "us-west-2"
    dynamodb_table = "pvl-terraform-dynamodb-table-qa"
  }
}

// creating ecr ui repo
resource "aws_ecr_repository" "ecr-ui" {
  name                 = "${var.org_prefix}-${var.ecr_repo_ui_name}-${var.environment}"
  
  image_scanning_configuration {
    scan_on_push = false
  }
}

// creating ecr api repo
resource "aws_ecr_repository" "ecr-api" {
  name                 = "${var.org_prefix}-${var.ecr_repo_api_name}-${var.environment}"
  image_scanning_configuration {
    scan_on_push = false
  }
}
