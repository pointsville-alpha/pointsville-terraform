// IAM service role for cicd
resource "aws_iam_role" "cicd-iam-role" {
  name = "${var.org_prefix}-cicd-iam-role-${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codestar.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }    
  ]
}
EOF
}

// IAM policy for cicd
resource "aws_iam_role_policy" "pv-iam-policy" {
  role = aws_iam_role.cicd-iam-role.name
  depends_on    = [ 
    aws_s3_bucket.codepipeline-bucket ]
  policy = <<POLICY
{
	"Version": "2012-10-17",
	"Statement": [
    {
		  "Effect": "Allow",
		  "Resource": "*",
		  "Action": [
		    "logs:CreateLogGroup",
		    "logs:CreateLogStream",
		    "logs:PutLogEvents"
		  ]
	  },
	  {
		  "Effect": "Allow",
		  "Action": "s3:*",
		  "Resource": [
        "${aws_s3_bucket.codepipeline-bucket.arn}",
        "${aws_s3_bucket.codepipeline-bucket.arn}/*"
		  ]
	  },
    {
      "Effect": "Allow",
      "Action": "ecr:*",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "ecs:*",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "codebuild:*",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "codedeploy:*",
      "Resource": "*"
    },    
    {
      "Sid": "ConnectionsFullAccess",
      "Effect": "Allow",
      "Action": "codestar-connections:*",
      "Resource": "*"
    }
	]
}
POLICY
}

// IAM policy for cicd
resource "aws_iam_role_policy" "pv-iam-policy-2" {
  role = aws_iam_role.cicd-iam-role.name
  depends_on    = [ 
    aws_s3_bucket.codepipeline-bucket]
  policy = <<POLICY
{
    "Statement": [
        {
            "Action": [
                "iam:PassRole"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Condition": {
                "StringEqualsIfExists": {
                    "iam:PassedToService": [
                        "cloudformation.amazonaws.com",
                        "elasticbeanstalk.amazonaws.com",
                        "ec2.amazonaws.com",
                        "ecs-tasks.amazonaws.com"
                    ]
                }
            }
        },
        {
            "Action": [
                "codedeploy:CreateDeployment",
                "codedeploy:GetApplication",
                "codedeploy:GetApplicationRevision",
                "codedeploy:GetDeployment",
                "codedeploy:GetDeploymentConfig",
                "codedeploy:RegisterApplicationRevision"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "codestar-connections:UseConnection"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "elasticbeanstalk:*",
                "ec2:*",
                "elasticloadbalancing:*",
                "autoscaling:*",
                "cloudwatch:*",
                "s3:*",
                "sns:*",
                "cloudformation:*",
                "rds:*",
                "sqs:*",
                "ecs:*"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "lambda:InvokeFunction",
                "lambda:ListFunctions"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "codebuild:BatchGetBuilds",
                "codebuild:StartBuild",
                "codebuild:BatchGetBuildBatches",
                "codebuild:StartBuildBatch"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Effect": "Allow",
            "Action": [
                "devicefarm:ListProjects",
                "devicefarm:ListDevicePools",
                "devicefarm:GetRun",
                "devicefarm:GetUpload",
                "devicefarm:CreateUpload",
                "devicefarm:ScheduleRun"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "servicecatalog:ListProvisioningArtifacts",
                "servicecatalog:CreateProvisioningArtifact",
                "servicecatalog:DescribeProvisioningArtifact",
                "servicecatalog:DeleteProvisioningArtifact",
                "servicecatalog:UpdateProduct"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudformation:ValidateTemplate"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ecr:DescribeImages"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "states:DescribeExecution",
                "states:DescribeStateMachine",
                "states:StartExecution"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "appconfig:StartDeployment",
                "appconfig:StopDeployment",
                "appconfig:GetDeployment"
            ],
            "Resource": "*"
        }
    ],
    "Version": "2012-10-17"
}
POLICY
}
// codebuild project for admin ui
resource "aws_codebuild_project" "ui-codebuild" {
  depends_on    = [ 
    aws_s3_bucket.codepipeline-bucket
  ]
  name          = "${var.org_prefix}-uicodebuild-${var.environment}"
  build_timeout = "60"
  service_role  = aws_iam_role.cicd-iam-role.arn
  artifacts {
    type                        = "CODEPIPELINE"
    override_artifact_name      = true
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode = true
  }

  source {
    type = "CODEPIPELINE"
    buildspec                   = "buildspec.yml"
  }

  tags = {
    Environment = var.environment
  }

    logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }
  }
}

// codepipeline for admin ui module
resource "aws_codepipeline" "admin-ui-pipeline" {

  depends_on = [ aws_codebuild_project.ui-codebuild ]

  name     = "${var.org_prefix}-admin-ui-pipeline-${var.environment}"
  role_arn = aws_iam_role.cicd-iam-role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline-bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        ConnectionArn = "arn:aws:codestar-connections:us-west-2:611541358627:connection/2f9fcaaf-f340-406c-963b-7d5b8343048b"
        FullRepositoryId = "pointsville-beta/pointsville-admin-app"
        BranchName = var.bitbucket_branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.ui-codebuild.name
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "ECS"
      version         = 1
      run_order       = 1
      input_artifacts = ["build_output"]

      configuration = {
        ClusterName = aws_ecs_cluster.ecs_cluster.name
        ServiceName = aws_ecs_service.ui-service.name
      }
    }
  }
  
}

// codebuild project for admin backend module
resource "aws_codebuild_project" "backend-codebuild" {
  depends_on    = [ 
    aws_s3_bucket.codepipeline-bucket
  ]

  name                          = "${var.org_prefix}-backendcodebuild-${var.environment}"
  build_timeout                 = "60"
  service_role                  = aws_iam_role.cicd-iam-role.arn
  artifacts {
    type                        = "CODEPIPELINE"
    override_artifact_name      = true
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true
  }
  source {
    type                        = "CODEPIPELINE"
    buildspec                   = "buildspec.yml"
  }
  tags = {
    Environment                 = var.environment
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }
  }

}

// codepipeline for admin backend module
resource "aws_codepipeline" "admin-backend-pipeline" {

  depends_on = [ aws_codebuild_project.backend-codebuild ]

  name     = "${var.org_prefix}-admin-backend-pipeline-${var.environment}"
  role_arn = aws_iam_role.cicd-iam-role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline-bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        ConnectionArn = "arn:aws:codestar-connections:us-west-2:611541358627:connection/2f9fcaaf-f340-406c-963b-7d5b8343048b"
        FullRepositoryId = "pointsville-beta/pointsville-admin-backend"
        BranchName = var.bitbucket_branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.backend-codebuild.name
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "ECS"
      version         = 1
      run_order       = 1
      input_artifacts = ["build_output"]

      configuration = {
        ClusterName = aws_ecs_cluster.ecs_cluster.name
        ServiceName = aws_ecs_service.api-service.name
      }
    }
  }
}

// codebuild project for android
resource "aws_codebuild_project" "android-codebuild" {
  depends_on    = [ 
    aws_s3_bucket.codepipeline-bucket
  ]

  name                          = "${var.org_prefix}-androidcodebuild-${var.environment}"
  build_timeout                 = "60"
  service_role                  = aws_iam_role.cicd-iam-role.arn
  artifacts {
    type                        = "CODEPIPELINE"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_MEDIUM"
    image                       = "aws/codebuild/standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true
  }
  source {
    type                      = "CODEPIPELINE"
    buildspec                   = "android-buildspec.yml"
  }
  tags = {
    Environment                 = var.environment
  }

    logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }
  }
}


// codepipeline for android 
resource "aws_codepipeline" "android-pipeline" {

  depends_on = [ aws_codebuild_project.backend-codebuild ]

  name     = "${var.org_prefix}-android-pipeline-${var.environment}"
  role_arn = aws_iam_role.cicd-iam-role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline-bucket.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      version          = "1"
      output_artifacts = ["source_output"]

      configuration = {
        ConnectionArn = "arn:aws:codestar-connections:us-west-2:611541358627:connection/2f9fcaaf-f340-406c-963b-7d5b8343048b"
        FullRepositoryId = "pointsville-beta/pointsville-mobile"
        BranchName = var.bitbucket_branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      version          = "1"

      configuration = {
        ProjectName = aws_codebuild_project.android-codebuild.name
      }
    }
  }
  }
  