// creating ecs cluster
resource "aws_ecs_cluster" "ecs_cluster" {
    name = "${var.org_prefix}-${var.environment}"
    capacity_providers = ["FARGATE_SPOT", "FARGATE"]

    default_capacity_provider_strategy {
        capacity_provider = "FARGATE_SPOT"
    }

    setting {
        name  = "containerInsights"
        value = "disabled"
    }
    tags = {
        Name = "${var.org_prefix}-${var.environment}"
        Environment = var.environment
    }
}

// ecs taskdefinition for ui containers
resource "aws_ecs_task_definition" "ui_taskdef" {
    family                   = "${var.org_prefix}-ui-taskdef-${var.environment}" //"ui-taskdef"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = var.ui_task_cpu
    memory                   = var.ui_task_memory
    execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
    task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
    container_definitions = jsonencode([
    {
      essential    = true
      image        = "${var.ecr_ui_repository_url}:latest"
      name         = var.ui_container_name
      portMappings = var.ui_container_port_mappings

      environment : [{
          name: "DB_HOSTNAME",
          value: aws_db_instance.pointsville-rds-db-instance.endpoint
      }]

    }
  ])
    tags = {
        Name = "${var.org_prefix}-ui-taskdef-${var.environment}"
        Environment = var.environment
    }
}

// ecs taskdefinition for api containers
resource "aws_ecs_task_definition" "api_taskdef" {
    family                   = "${var.org_prefix}-api-taskdef-${var.environment}" //"api-taskdef"
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = var.api_task_cpu
    memory                   = var.api_task_memory
    execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
    task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
    container_definitions = jsonencode([
    {
      essential              = true
      image                  = "${var.ecr_api_repository_url}:latest"
      name                   = var.api_container_name
      portMappings           = var.api_container_port_mappings
       
      environment : [{
          name: "DB_HOSTNAME",
          value: aws_db_instance.pointsville-rds-db-instance.endpoint
      }]
              
      logConfiguration       = var.logConfiguration_api
    }
])
    
    tags = {
        Name = "${var.org_prefix}-api-taskdef-${var.environment}"
        Environment = var.environment
    }
}

// task execution role for ECS tasks
resource "aws_iam_role" "ecs_task_execution_role" {
    name = "${var.org_prefix}-ecsTaskExecutionRole-${var.environment}"
    assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ecs-tasks.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

// policy attachment for task execution role
resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
    role       = aws_iam_role.ecs_task_execution_role.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

// ecs ui service
resource "aws_ecs_service" "ui-service" {
    name                               = "${var.org_prefix}-ui-svc-${var.environment}"
    cluster                            = aws_ecs_cluster.ecs_cluster.id
    task_definition                    = aws_ecs_task_definition.ui_taskdef.arn
    desired_count                      = 2
    deployment_minimum_healthy_percent = 50
    deployment_maximum_percent         = 200
    launch_type                        = "FARGATE"
    scheduling_strategy                = "REPLICA"
 
    network_configuration {
        security_groups  = [ aws_security_group.pvl-ui-svc-sg.id ]
        subnets          = aws_subnet.pointsville-public-subnet.*.id
        assign_public_ip = true
    }
 
    load_balancer {
        target_group_arn = aws_alb_target_group.ui_tg.arn
        container_name   = var.ui_container_name
        container_port   = var.ui_container_port 
    }
    depends_on = [ aws_alb_target_group.ui_tg, aws_ecs_task_definition.ui_taskdef, aws_ecs_cluster.ecs_cluster, aws_lb.ui_lb ]
}

// ECS api service 
resource "aws_ecs_service" "api-service" {
    name                               = "${var.org_prefix}-api-svc-${var.environment}"
    cluster                            = aws_ecs_cluster.ecs_cluster.id
    task_definition                    = aws_ecs_task_definition.api_taskdef.arn
    desired_count                      = 2
    deployment_minimum_healthy_percent = 50
    deployment_maximum_percent         = 200
    launch_type                        = "FARGATE"
    scheduling_strategy                = "REPLICA"
 
    network_configuration {
        security_groups  = [ aws_security_group.pvl-api-svc-sg.id ]
        subnets          =  aws_subnet.pointsville-public-subnet.*.id
        assign_public_ip = true
    }
 
    load_balancer {
        target_group_arn = aws_alb_target_group.api_tg.arn
        container_name   = var.api_container_name
        container_port   = var.api_container_port 
    }
    depends_on = [ aws_alb_target_group.api_tg, aws_ecs_task_definition.api_taskdef, aws_ecs_cluster.ecs_cluster, aws_lb.api_lb ]
}

//Cloudwatch log group for API Tasks
resource "aws_cloudwatch_log_group" "api-task-logs" {
  name = "awslogs-api"
  tags = {
    Environment = var.environment
  }
}