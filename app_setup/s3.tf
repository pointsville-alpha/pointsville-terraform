//creating s3 bucket for Codepipeline
resource "aws_s3_bucket" "codepipeline-bucket" {
    bucket = "${var.org_prefix}-codepipeline-${var.environment}"
    acl = "private"
    force_destroy = true

    tags = {
        Name = "${var.org_prefix}-codepipeline-${var.environment}"
        Environment = var.environment
    }
}