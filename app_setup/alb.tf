// ui application loadbalancer
resource "aws_lb" "ui_lb" {
    name               = "${var.org_prefix}-ui-alb-${var.environment}"
    internal           = false
    load_balancer_type = "application"
    security_groups    = [aws_security_group.pvl-ui-alb-sg.id]
    subnets            = aws_subnet.pointsville-public-subnet.*.id
    enable_deletion_protection = false
}
 
// ui target group
resource "aws_alb_target_group" "ui_tg" {
    name        = "${var.org_prefix}-ui-tg-${var.environment}"
    port        = 80
    protocol    = "HTTP"
    vpc_id      = aws_vpc.pointsville-vpc.id
    target_type = "ip"
    
    health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
    }
}

// http listener for UI load balancer
resource "aws_alb_listener" "http_listener_ui" {
    load_balancer_arn = aws_lb.ui_lb.arn
    port              = 80
    protocol          = "HTTP"

    default_action {
    target_group_arn = aws_alb_target_group.ui_tg.arn
    type             = "forward"
   }
}
 
// https listener for ui loadbalancer
# resource "aws_alb_listener" "https_listener_ui" {
#   load_balancer_arn = aws_lb.main.id
#   port              = 443
#   protocol          = "HTTPS"
 
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = var.alb_tls_cert_arn
 
#   default_action {
#     target_group_arn = aws_alb_target_group.ui_tg.arn
#     type             = "forward"
#   }
# }

####################################################
// api application loadbalancer
resource "aws_lb" "api_lb" {
    name               = "${var.org_prefix}-api-alb-${var.environment}"
    internal           = false
    load_balancer_type = "application"
    security_groups    = [ aws_security_group.pvl-api-alb-sg.id ]
    subnets            =  aws_subnet.pointsville-public-subnet.*.id
    enable_deletion_protection = false
    depends_on = [ aws_vpc.pointsville-vpc, aws_subnet.pointsville-public-subnet]
}
 
// api target group
resource "aws_alb_target_group" "api_tg" {
    name        = "${var.org_prefix}-api-tg-${var.environment}"
    port        = 80
    protocol    = "HTTP"
    vpc_id      = aws_vpc.pointsville-vpc.id
    target_type = "ip"
    
    health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "404" #"200-299"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
    }
}

// http listener
resource "aws_alb_listener" "http_listener_api" {
    load_balancer_arn = aws_lb.api_lb.arn
    port              = 80
    protocol          = "HTTP"

    default_action {
    target_group_arn = aws_alb_target_group.api_tg.arn
    type             = "forward"
   }
}
 
// https listener 
# resource "aws_alb_listener" "https_listener_api" {
#   load_balancer_arn = aws_lb.api_lb.arn
#   port              = 443
#   protocol          = "HTTPS"
 
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = var.alb_tls_cert_arn
 
#   default_action {
#     target_group_arn = aws_alb_target_group.api_tg.arn
#     type             = "forward"
#   }
# }

//WAF Module for to implement WAF Rules
module "waf_regional_rules" {
  source = "./modules/waf"
}

#WAF REGIONAL WEB ACL DEFINITION
resource "aws_wafregional_web_acl" "waf_acl" {
  depends_on = [
    module.waf_regional_rules.blacklistips,
    module.waf_regional_rules.detect_blacklistips,
    module.waf_regional_rules.sqlInjection
  ]
  name        = "${var.org_prefix}-WebACL-${var.environment}" //"tfWebACL"
  metric_name = "tfWebACL"

  default_action {
    type = "ALLOW"
  }
  rule {
    action {
      type = "BLOCK"
    }

    priority = 2
    rule_id = module.waf_regional_rules.sqlInjection.id
    type    = "REGULAR"
  }
  rule {
    action {
      type = "BLOCK"
    }
    priority = 1
    rule_id = module.waf_regional_rules.detect_blacklistips.id
    type    = "REGULAR"
  }
}

//Web ACL association with Application Load Balancer
resource "aws_wafregional_web_acl_association" "alb" {
 depends_on = [
    aws_wafregional_web_acl.waf_acl,
    aws_lb.ui_lb
  ]
  resource_arn = aws_lb.ui_lb.arn #ARN of the ALB
  web_acl_id   = aws_wafregional_web_acl.waf_acl.id
}