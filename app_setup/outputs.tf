output "rds-connection-string" {
    value = aws_db_instance.pointsville-rds-db-instance.endpoint
}

output "pointsville-lb-url" {
    value = aws_lb.ui_lb.dns_name
}

output "ui-ecr-repository-url" {
  value = aws_ecr_repository.ecr-ui.repository_url
}

output "api-ecr-repository-url" {
  value = aws_ecr_repository.ecr-api.repository_url
}


