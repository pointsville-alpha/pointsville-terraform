region_name     = "us-west-2"
environment     = "qa-terraform"
org_prefix      = "pointsville"
vpc_cidr_block  = "10.0.0.0/16"
open_cidr   = "0.0.0.0/0"

public_subnet_cidr_block1   = "10.0.1.0/24"
private_subnet_cidr_block1  = "10.0.111.0/24"
public_subnet_cidr_block2   = "10.0.2.0/24"
private_subnet_cidr_block2  = "10.0.112.0/24"
public_subnet_cidr_block3   = "10.0.3.0/24"
private_subnet_cidr_block3  = "10.0.113.0/24"

backend_api_port = "4000"
ui_service_sg_name  = "ui-svc-sg"
ui_loadbalancer_sg_name = "ui-alb-sg"
api_service_sg_name = "api-svc-sg"
api_loadbalancer_sg_name = "api-alb-sg"
# artifacts_bucket_name = "ci-artifacts-test1"

db_identifier_name = "postgres-db"
db_username =   "postgres"
db_password = "password"
db_engine = "postgres"
db_engine_version = "12.3"
db_subnetgroup_name = "db-subnetgroup"
db_instance_class = "db.t3.micro"
db_name = "pointsville"
db_allocated_storage = "100"
db_port = "5543"


bitbucket_branch="qa-terraform"
bitbucket_username="sitaramconsultant"

//s3_artifact_bucket = "pointsville-artifacts-dev-test1"
//s3_artifact_ui_objectkey = "ui-artifacts/ui-imagedef.zip"
//s3_artifact_api_objectkey = "api-artifacts/api-imagedef.zip"

ecr_repo_ui_name = "uirepo"
ecr_repo_api_name = "apirepo"

sns_subscription_email_address_list_pipeline = "cloud@ggangireddy.com,sitaram.consultant@pointsville.com"
# sns_subscription_email_address_list_codebuild = "email1@pointsville.com"

ui_container_name = "frontend-container"
# ui_image_uri = ""
ui_container_port = "80"
api_container_name = "backend-container"
# api_image_uri = "611541358627.dkr.ecr.us-east-1.amazonaws.com/pvl-admin-api"
api_container_port = "4000"

//Note: These 2 URLs should be matching with resource "aws_ecr_repository"."ecr-ui" in main file
ecr_ui_repository_url = "611541358627.dkr.ecr.us-west-2.amazonaws.com/pointsville-uirepo-qa-terraform"
ecr_api_repository_url = "611541358627.dkr.ecr.us-west-2.amazonaws.com/pointsville-apirepo-qa-terraform"

ui_task_cpu = 256
ui_task_memory = 512
api_task_cpu = 1024
api_task_memory = 2048

user_pool_name = "pointsville_userpool_qa_terraform"
sns_caller_arn = "arn:aws:iam::611541358627:role/sns55d2d438210945-dev"
sns_external_id = "points55d2d438_role_external_id"
lambda_auth_challenge = "arn:aws:lambda:us-east-1:611541358627:function:pointsvilleapp55d2d438CustomMessage-dev"
verify_auth_challenge_response = "arn:aws:lambda:us-east-1:611541358627:function:pointsvilleapp55d2d438CustomMessage-dev"

state_file_bucket_name = "pvl-terraform-statefile-qa"
dynamo_db_table_name = "pvl-terraform-dynamodb-table-qa"
#ecr_repository_name = ""


