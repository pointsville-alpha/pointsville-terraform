
# // creating ui service security group
resource "aws_security_group" "pvl-ui-svc-sg" {
    name = "${var.org_prefix}-${var.ui_service_sg_name}-${var.environment}"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.open_cidr]
    }
    egress {
        protocol = "-1"
        from_port = 0
        to_port = 0
        cidr_blocks = [var.open_cidr]
    }

    vpc_id = aws_vpc.pointsville-vpc.id

    tags  = {
        Name = "${var.org_prefix}-${var.ui_service_sg_name}-${var.environment}"
        Environment = var.environment
    }
}

// Allowing 80 port to api alb security group
resource "aws_security_group_rule" "allow-80-api-alb-ui-svc" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-api-alb-sg.id
    security_group_id         = aws_security_group.pvl-ui-svc-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-api-alb-sg ]
}

// Allowing 80 port to ui alb security group
resource "aws_security_group_rule" "allow-80-ui-alb-ui-svc" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-alb-sg.id
    security_group_id         = aws_security_group.pvl-ui-svc-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-ui-alb-sg ]
}

// api service security group
resource "aws_security_group" "pvl-api-svc-sg" {
    name = "${var.org_prefix}-${var.api_service_sg_name}-${var.environment}"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }
    ingress {
        from_port = var.backend_api_port
        to_port = var.backend_api_port
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr_block]
    }
    egress {
        protocol = "-1"
        from_port = 0
        to_port = 0
        cidr_blocks = [var.open_cidr]
    }

    vpc_id = aws_vpc.pointsville-vpc.id

    tags    = {
        Name = "${var.org_prefix}-${var.api_service_sg_name}-${var.environment}"       
        Environment = var.environment
    }
}

// Allowing 80 port to ui service security group
resource "aws_security_group_rule" "allow-80-ui-svc-api-svc" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-svc-sg.id
    security_group_id         = aws_security_group.pvl-api-svc-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-api-svc-sg ]
}

// Allowing 80 port to ui alb security group
resource "aws_security_group_rule" "allow-80-ui-alb-api-svc" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-alb-sg.id
    security_group_id         = aws_security_group.pvl-api-svc-sg.id
    depends_on = [ aws_security_group.pvl-ui-alb-sg, aws_security_group.pvl-api-svc-sg ]
}

// Allowing backend port to ui alb security group
resource "aws_security_group_rule" "allow-4000-ui-alb-api-svc" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-alb-sg.id
    security_group_id         = aws_security_group.pvl-api-svc-sg.id
    depends_on = [ aws_security_group.pvl-ui-alb-sg, aws_security_group.pvl-api-svc-sg ]
}

// Allowing backend port to api alb security group
resource "aws_security_group_rule" "allow-4000-ui-svc-api-svc" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-api-alb-sg.id
    security_group_id         = aws_security_group.pvl-api-svc-sg.id
    depends_on = [ aws_security_group.pvl-api-alb-sg, aws_security_group.pvl-api-svc-sg ]
}

// creating api alb security group
resource "aws_security_group" "pvl-api-alb-sg" {
    name = "${var.org_prefix}-${var.api_loadbalancer_sg_name}-${var.environment}"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.open_cidr]
    }
    
    egress {
        protocol = "-1"
        from_port = 0
        to_port = 0
        cidr_blocks = [var.open_cidr]
    }

    vpc_id = aws_vpc.pointsville-vpc.id

    tags = {
        Name = "${var.org_prefix}-${var.api_loadbalancer_sg_name}-${var.environment}"
        Environment = var.environment
    }
}

// Allowing 80 port  ui service security group
resource "aws_security_group_rule" "allow-80-ui-svc-api-alb" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-svc-sg.id
    security_group_id         = aws_security_group.pvl-api-alb-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-api-alb-sg ]

}
// Allowing 80 port to ui alb security group
resource "aws_security_group_rule" "allow-80-ui-alb-api-alb" {
    type                      = "ingress"
    from_port                 = 80
    to_port                   = 80
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-alb-sg.id
    security_group_id         = aws_security_group.pvl-api-alb-sg.id
    depends_on = [ aws_security_group.pvl-ui-alb-sg, aws_security_group.pvl-api-alb-sg ]
}
// Allowing backend port to ui service security group
resource "aws_security_group_rule" "allow-4000-ui-svc-api-alb" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-svc-sg.id
    security_group_id         = aws_security_group.pvl-api-alb-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-api-alb-sg ]
}

// Allowing backend port to ui alb security group
resource "aws_security_group_rule" "allow-4000-ui-alb-api-alb" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-alb-sg.id
    security_group_id         = aws_security_group.pvl-api-alb-sg.id
    depends_on = [ aws_security_group.pvl-ui-alb-sg, aws_security_group.pvl-api-alb-sg ]
}
// Allowing backend port to internal vpc ips
resource "aws_security_group_rule" "allow-4000-internal" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    cidr_blocks               = [var.vpc_cidr_block]
    security_group_id         = aws_security_group.pvl-api-alb-sg.id
    depends_on = [ aws_security_group.pvl-api-alb-sg ]

}

// creating ui alb security group
resource "aws_security_group" "pvl-ui-alb-sg" {
    name = "${var.org_prefix}-${var.ui_loadbalancer_sg_name}-${var.environment}"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.open_cidr]
    }
    
    egress {
        protocol = "-1"
        from_port = 0
        to_port = 0
        cidr_blocks = [var.open_cidr]
    }

    vpc_id = aws_vpc.pointsville-vpc.id

    tags = {
        Name = "${var.org_prefix}-${var.ui_loadbalancer_sg_name}-${var.environment}"
        Environment = var.environment
    }
}

// Allowing backend port to ui service security group
resource "aws_security_group_rule" "allow-4000-ui-svc-ui-alb" {
    type                      = "ingress"
    from_port                 = var.backend_api_port
    to_port                   = var.backend_api_port
    protocol                  = "tcp"
    source_security_group_id  = aws_security_group.pvl-ui-svc-sg.id
    security_group_id         = aws_security_group.pvl-ui-alb-sg.id
    depends_on = [ aws_security_group.pvl-ui-svc-sg, aws_security_group.pvl-ui-alb-sg ]
}